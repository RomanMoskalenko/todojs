let arr = new Array();
let state = 'All';

function createTodoItem(title, isChecked) {
    const checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkbox.classList.add('checkbox');
    checkbox.classList.add('toggle');
    checkbox.checked = isChecked;

    const label1 = document.createElement('label');

    const label = document.createElement('label');
    label.innerText = title;
    label.className = 'title';

    const editInput = document.createElement('input')
    editInput.type = 'text';
    editInput.className = 'textfield';

    const closeSpan = document.createElement('span')
    closeSpan.innerText = '\u00D7';
    closeSpan.className = 'close';

    const listItem = document.createElement('li');
    listItem.className = 'todo-item';

    listItem.appendChild(checkbox);
    listItem.appendChild(label1);
    listItem.appendChild(label);
    listItem.appendChild(closeSpan);
    listItem.appendChild(editInput);
    // listItem.appendChild(editButton);
    // listItem.appendChild(deleteButton);   


    bindEvents(listItem);
    
    return listItem;
}

function createAsideItem(title, isChecked) {
	const aEl = document.createElement('a');
	aEl.innerText = title;
	aEl.className = 'nav-link';
	
	const divEl = document.createElement('div');
	if (isChecked) {
		divEl.className = 'active';
	}
	
	divEl.appendChild(aEl);
    
    return divEl;
}

function bindEvents(todoItem) {
    const checkbox = todoItem.querySelector('.checkbox');
    //const editButton = todoItem.querySelector('button.edit');
    //console.log("editButton object" + editButton);
    //const deleteButton = todoItem.querySelector('button.delete');
    
    //console.log(editButton);

    checkbox.addEventListener('change', toggleTodoItem);
    //editButton.addEventListener('click', editTodoItem);
    //deleteButton.addEventListener('click', deleteTodoItem);
}

function addTodoItem(event) {
    event.preventDefault();

    if (addInput.value === '') 
        return alert("you need input name of task");

    const todoItem = createTodoItem(addInput.value, false);

    arr.push({
        text : addInput.value,
        isChecked : false
    });

    todoList.appendChild(todoItem);
    addInput.value = '';
}

function toggleTodoItem() {
    const listItem = this.parentNode;
    listItem.classList.toggle('completed');
    filterLink(state);
}

function editTodoItem() {
    const listItem = this.parentNode;
    const title = listItem.querySelector('.title');
    const editInput = listItem.querySelector('.textfield');
    const isEditing = listItem.classList.contains('editing');

    if (isEditing) {
        title.innerText = editInput.value;
        this.innerText = 'change';
    } else {
        editInput.value = title.innerText;
        this.innerText = 'save';
    }

    listItem.classList.toggle('editing');
}

function deleteTodoItem() {
    const listItem = this.parentNode;
    todoList.removeChild(listItem);
}

function filterLink(name) {
    todoItems = document.querySelectorAll('.todo-item');
    if (name == 'All') {
        for (var i=0; i < todoItems.length; i++) {
            todoItems[i].style.display = 'flex';
        }
    } else if (name == 'Active') {        
        for (var i=0; i < todoItems.length; i++) {
            if (todoItems[i].classList.contains('completed'))
                todoItems[i].style.display = 'none';
            else 
                todoItems[i].style.display = 'flex';
        }
    }  else if (name == 'Completed') {
        for (var i=0; i < todoItems.length; i++) {
            if (!todoItems[i].classList.contains('completed'))
                todoItems[i].style.display = 'none';
            else 
                todoItems[i].style.display = 'flex';
        }
    }
}

function createtodolists(data) {
	for(let i=0;i < data.length; i++) {
		console.log(data[i]);
		let item = '';
		if (i == 0)
		item = createAsideItem(data[i].name, true);
		else
		item = createAsideItem(data[i].name, false);
		todoLists.appendChild(item);
	}
}

const todoForm = document.getElementById('todo-form');
const addInput = document.getElementById('add-input');
const todoList = document.getElementById('todo-list');
var todoItems = document.querySelectorAll('.todo-item');
var todoLists = document.getElementById('nav-pills');
todoItems = '';

function main() {
	var data = '';	
    
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
	       // Typical action to be performed when the document is ready:
		data = JSON.parse(xhttp.responseText);
		createtodolists(data);
		console.log(data);
		data = data[0].items;
		for(let i=0;i < data.length; i++) {
				console.log(data[i]);
				const todoItem = createTodoItem(data[i].text, data[i].done);
				todoList.appendChild(todoItem);
		}
	    }
	};
	xhttp.open("GET", "http://localhost:8080/api/list", true);
	xhttp.send();
	

    document.getElementById('allLink')
        .addEventListener('click', function() {
            filterLink('All')
            state = 'All';
        });

    document.getElementById('selectedLink')
        .addEventListener('click', function() {
            filterLink('Active')
            state = 'Active'
        });

    document.getElementById('completedLink')
        .addEventListener('click', function() {
            filterLink('Completed')
            state = 'Completed';
        });

    todoForm.addEventListener('submit', addTodoItem);
    
}

document.addEventListener('DOMContentLoaded', main);
